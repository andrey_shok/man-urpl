using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace glp_compiler
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Analysis analysis=new Analysis("Hello.upl");
			Console.WriteLine(analysis.Start("Hello.upl"));
		}
	}
	class Analysis
	{	
		private string nameFile;
		//Список слов в текущей строке. 
		private List<string> current_String=new List<string>();

		//Конструктор по умолчанию
		public Analysis(string nameFile)
		{
			this.nameFile = nameFile;
		}
		//Общедоступная ф-ция для запуска анализа и вывода ошибок
		public string Start(string nameFile)
		{
			return Analysis_syntax (this.nameFile);
		}
		//Возвращает текущюю строку, листом
		private bool  Get_Next_String (string currentString )
		{
			if (currentString!=null){
				string timeWord = "";
				current_String.Clear ();
				int i;

				for (i=0; i<currentString.Length; i++) {
					if ((currentString [i] != '(') && (currentString[i]!=')')&& (currentString[i]!=';')&& (currentString[i]!=' ')
					    &&(currentString[i]!='\t')&&(currentString[i]!='\n')&&(currentString[i]!=''))
							timeWord += currentString [i];
					else {
						if (currentString [i] == '\t')
							current_String.Add("\t");
						if(timeWord!="")
							current_String.Add (timeWord);

						timeWord = "";
					}
				}

				current_String.Add (timeWord);

				return true;
			}
		
		else 
			return false;
		}
		private string Analysis_If(){
			string cstr="";
			int i=0;

			while (i!=current_String.Count) {

					switch (current_String [i]) {
					case "\t":
						cstr += "\t";
						i += 1;
						
						break;
					case "Якщо":
						cstr += "if(" + current_String [i + 1] + ")";
						i += 2;
						
						break;
					case "або":
						cstr += "or(" + current_String [i + 1] + ")";
						i += 2;
						
						break;
					case "та":
						cstr += "and(" + current_String [i + 1] + ")";
						i += 2;
						
						break;
					case "тоді":
						cstr += ":";
						i = current_String.Count;
						
						break;
				default:
					i++;
					break;
					}
				}

			return(cstr);
		}

		private string Analysis_For(){
			string cstr="";
			return(cstr);
		}

		private string Analysis_While(){
			string cstr="";
			return(cstr);
		}

		private string AllPrint(int k){
			string cstr="";
			int i;
			for (i=0; i<k; i++) {
				cstr += current_String [i];
			}
			for (i=k; i<current_String.Count; i++) {
				cstr += current_String [i]+" ";
			}
			return(cstr);
		}


		//ф-ция отвечает за анализ синтаксиса, прежде чем компилировать


		private string Analysis_syntax(string nameFile)
		{
			string streamString="";
			//Получение строки по словам. Выполнено здесь что-бы постоянно не дергать файл.
			using (StreamReader streamReader = new StreamReader(this.nameFile)) {

				using ( StreamWriter sw = new StreamWriter("python.py")) {

				
					while (Get_Next_String (streamReader.ReadLine ())) {
						int num;
						int i = 0;
					for (num=0; num<current_String.Count; num++) {						
							if (current_String [num] != "\t") {
								i = num;
								break;
							}
					}
						switch (current_String [i]) {
						
						case "Якщо":
							streamString=Analysis_If ();
							break;
						case "Від":
							streamString=Analysis_For ();
							break;
						case "Поки":
							streamString=Analysis_While ();
							break;
						default:
							streamString = AllPrint (i);
							break;
						}
						sw.WriteLine (streamString);

					}
					Console.WriteLine ("Done");
				}
			}
			return nameFile;
		}

	}
}
